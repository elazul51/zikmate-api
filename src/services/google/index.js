import request from 'request-promise'

export const getUser = accessToken =>
  request({
    uri: 'https://www.googleapis.com/userinfo/v2/me',
    json: true,
    qs: {
      access_token: accessToken
    }
  }).then(({ id, given_name, family_name, email, picture }) => ({
    service: 'google',
    picture,
    id,
    username: 'user-' + Math.floor(Math.random() * 1000000),
    firstname: given_name,
    lastname: family_name,
    email,
    verified: true
  }))
