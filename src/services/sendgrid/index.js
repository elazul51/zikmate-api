import sendgrid, { mail as helper } from 'sendgrid'
import { sendgridKey, defaultEmail, appFrontUrl } from '../../config'

export const sendMail = ({
  fromEmail = defaultEmail,
  toEmail,
  subject,
  content,
  contentType = 'text/html'
}) => {
  fromEmail = new helper.Email(fromEmail)
  toEmail = new helper.Email(toEmail)
  content = new helper.Content(contentType, content)
  const mail = new helper.Mail(fromEmail, subject, toEmail, content)
  const sg = sendgrid(sendgridKey)
  const request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON()
  })

  return sg.API(request)
}

export const sendUserVerifyMail = (user, token) => {
  const link = `${appFrontUrl}/users/verify/${token}`
  const content = `
    Bonjour ${user.username},<br><br>
    Tu es seulement à un clic de rejoindre la communauté Zikmate.<br>
    Pour confirmer ton inscription, clique simplement sur le lien suivant:<br><br>
    <a href="${link}">${link}</a><br><br>
    &mdash; L'équipe Zikmate
  `
  return sendMail({
    toEmail: user.email,
    subject: "Zikmate - Demande de confirmation d'inscription au service",
    content
  })
}

export const sendWelcomeMail = user => {
  const content = `
    Bonjour ${user.username},<br><br>
    Félicitations, ton inscription est à présent complète!<br>
    Tu peux dès maintenant rejoindre la communauté Zikmate en te connectant avec tes identifiants à l'adresse suivante:<br><br>
    <a href="${appFrontUrl}">${appFrontUrl}</a><br><br>
    &mdash; L'équipe Zikmate
  `
  return sendMail({
    toEmail: user.email,
    subject: 'Zikmate - Bienvenue dans la communauté Zikmate!',
    content
  })
}

export const sendPasswordResetMail = (user, token) => {
  const link = `${appFrontUrl}/password-reset/${token}`
  const content = `
    Bonjour ${user.username},<br><br>
    Tu as fait une demande de changement de mot de passe sur ton compte Zikmate.<br>
    Utilise le lien suivant pour définir un nouveau mot de passe. Ce lien expirera dans une heure.<br><br>
    <a href="${link}">${link}</a><br><br>
    Si toutefois tu n'es pas à l'origine de cette demande, tu peux ignorer ce message sans problème. :)<br><br>
    &mdash; L'équipe Zikmate
  `
  return sendMail({
    toEmail: user.email,
    subject: 'Zikmate - Réinitialisation du mot de passe',
    content
  })
}

export const sendFriendRequest = (fromUser, toUser) => {
  const link = `${appFrontUrl}/friends`
  const content = `
    Bonjour ${toUser.username},<br><br>
    Tu as reçu une demande d'ami de la part de ${fromUser.username}.<br>
    Connecte-toi sur ton compte Zikmate pour accepter sa demande.<br><br>
    <a href="${link}">${link}</a><br><br>
    &mdash; L'équipe Zikmate
  `
  return sendMail({
    toEmail: toUser.email,
    subject: "Zikmate - Demande d'ajout à la liste d'amis",
    content
  })
}

export const sendPrivateMessage = (message, fromUser, toUser) => {
  const content = `
    Bonjour ${toUser.username},<br><br>
    Tu as reçu un message privé de la part de ${fromUser.username}:<br><br>
    <blockquote>${message}</blockquote>
  `
  return sendMail({
    toEmail: toUser.email,
    subject: `Zikmate - Message privé de ${fromUser.username}`,
    content
  })
}
