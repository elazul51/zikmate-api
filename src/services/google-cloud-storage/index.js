import crypto from 'crypto'
import fs from 'fs'
import nodecipher from 'node-cipher'
import {
  nodeCipherPassword,
  nodeCipherSalt,
  googleCloudServiceAccountFile,
  googleCloudStorageBucketName
} from '../../config'
import { Storage } from '@google-cloud/storage'

if (!fs.existsSync(googleCloudServiceAccountFile)) {
  nodecipher.decryptSync({
    input: `${googleCloudServiceAccountFile}.cast5`,
    output: googleCloudServiceAccountFile,
    password: nodeCipherPassword,
    salt: nodeCipherSalt
  })
}

const storage = new Storage()

export const uploadPicture = picture => {
  if (!picture) {
    return null
  }

  const filename = crypto.randomBytes(20).toString('hex')
  const filetype = picture.split(';')[0].split('/')[1]
  const buffer = Buffer.from(
    picture.replace(/^data:image\/\w+;base64,/, ''),
    'base64'
  )
  const file = storage
    .bucket(googleCloudStorageBucketName)
    .file(`pictures/${filename}`)

  return file
    .save(buffer, {
      metadata: {
        contentType: `image/${filetype}`
      },
      predefinedAcl: 'publicRead'
    })
    .then(() => {
      return file.get()
    })
    .then(uploadedFile => {
      return uploadedFile[0].getMetadata()
    })
    .then(metadatas => {
      return metadatas[0].mediaLink
    })
}
