import request from 'request-promise'

export const getUser = accessToken =>
  request({
    uri: 'https://graph.facebook.com/me',
    json: true,
    qs: {
      access_token: accessToken,
      fields: 'id, first_name, last_name, email, picture'
    }
  }).then(({ id, first_name, last_name, email, picture }) => ({
    service: 'facebook',
    picture: picture.data.url,
    id,
    username: 'user-' + Math.floor(Math.random() * 1000000),
    firstname: first_name,
    lastname: last_name,
    email,
    verified: true
  }))
