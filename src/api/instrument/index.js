import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { index, show, create, update, destroy } from './controller'
import { schema } from './model'
export Instrument, { schema } from './model'

const router = new Router()
const { name } = schema.tree

/**
 * @api {get} /instruments Retrieve instruments
 * @apiName RetrieveInstruments
 * @apiGroup Instrument
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiUse listParams
 * @apiSuccess {Object[]} instruments List of instruments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', token({ required: true }), query(), index)

/**
 * @api {get} /instruments/:id Retrieve instrument
 * @apiName RetrieveInstrument
 * @apiGroup Instrument
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess {Object} instrument Instrument's data.
 * @apiError 404 Instrument not found.
 */
router.get('/:id', token({ required: true }), show)

/**
 * @api {post} /instruments Create instrument
 * @apiName CreateInstrument
 * @apiGroup Instrument
 * @apiPermission admin
 * @apiParam {String} access_token User access_token.
 * @apiParam {String} name Instrument's name.
 * @apiSuccess (Sucess 201) {Object} instrument Instrument's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Admin access only.
 */
router.post(
  '/',
  token({ required: true, roles: ['admin'] }),
  body({ name }),
  create
)

/**
 * @api {put} /instruments/:id Update instrument
 * @apiName UpdateInstrument
 * @apiGroup Instrument
 * @apiPermission admin
 * @apiParam {String} access_token User access_token.
 * @apiParam {String} [name] Instrument's name.
 * @apiSuccess {Object} instrument Instrument's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Admin access only.
 * @apiError 404 Instrument not found.
 */
router.put(
  '/:id',
  token({ required: true, roles: ['admin'] }),
  body({ name }),
  update
)

/**
 * @api {delete} /instruments/:id Delete instrument
 * @apiName DeleteInstrument
 * @apiGroup Instrument
 * @apiPermission admin
 * @apiParam {String} access_token User access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 401 Admin access only.
 * @apiError 404 Instrument not found.
 */
router.delete('/:id', token({ required: true, roles: ['admin'] }), destroy)

export default router
