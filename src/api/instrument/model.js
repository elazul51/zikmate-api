import mongoose, { Schema } from 'mongoose'
import mongooseKeywords from 'mongoose-keywords'
import uniqueValidator from 'mongoose-unique-validator'

const instrumentSchema = new Schema(
  {
    name: {
      type: String,
      index: true,
      required: true,
      unique: true,
      trim: true,
      maxlength: [
        255,
        "Le nom de l'instrument doit contenir au maximum 255 caractères."
      ]
    }
  },
  {
    timestamps: true
  }
)

instrumentSchema.plugin(uniqueValidator, {
  message: '{VALUE} existe déjà'
})

instrumentSchema.methods = {
  view () {
    let view = {}
    const fields = ['id', 'name']

    fields.forEach(field => {
      view[field] = this[field]
    })

    return view
  }
}

instrumentSchema.plugin(mongooseKeywords, { paths: ['name'] })

const model = mongoose.model('Instrument', instrumentSchema)

export const schema = model.schema
export default model
