import { sign, verify } from '../../services/jwt'
import { success, notFound } from '../../services/response/'
import {
  sendUserVerifyMail,
  sendWelcomeMail,
  sendPrivateMessage
} from '../../services/sendgrid'
import { uploadPicture } from '../../services/google-cloud-storage'
import { User } from '.'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.countDocuments({
    ...query,
    _id: {
      $ne: query._id
    }
  })
    .then(totalCount => {
      return Promise.all([
        totalCount,
        User.find(
          {
            ...query,
            _id: {
              $ne: query._id
            }
          },
          select,
          cursor
        ).collation({ locale: 'fr' })
      ])
    })
    .then(([totalCount, users]) => {
      const result = {}
      result.data = users.map(user => user.view())
      result.totalCount = totalCount
      return result
    })
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then(user => (user ? user.view(true) : null))
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) => res.json(user.view(true))

export const create = ({ bodymen: { body } }, res, next) =>
  User.create(body)
    .then(user => {
      const options = {
        expiresIn: '1d'
      }
      return Promise.all([user, sign(user.id, options)])
    })
    .then(([user, token]) => {
      return Promise.all([user, sendUserVerifyMail(user, token)])
    })
    .then(([user]) => (user ? user.view(true) : null))
    .then(success(res))
    .catch(err => {
      /* istanbul ignore else */
      if (err.message) {
        res.status(409).json({
          valid: false,
          message: err.message
        })
      } else {
        next(err)
      }
    })

export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then(result => {
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin) {
        res.status(401).json({
          valid: false,
          message:
            "Vous ne pouvez pas modifier les données d'un autre utilisateur"
        })
        return null
      }
      return result
    })
    .then(user => {
      return Promise.all([user, uploadPicture(body.newPicture)])
    })
    .then(([user, picture]) => {
      delete body.newPicture
      if (picture) {
        user.picture = picture
      }
      return user
    })
    .then(user => (user ? Object.assign(user, body).save() : null))
    .then(user => (user ? user.view(true) : null))
    .then(success(res))
    .catch(next)

export const updatePassword = (
  { bodymen: { body }, params, user },
  res,
  next
) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then(result => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          valid: false,
          param: 'password',
          message:
            "Vous ne pouvez pas modifier le mot de passe d'un autre utilisateur"
        })
        return null
      }
      return result
    })
    .then(user => (user ? user.set({ password: body.password }).save() : null))
    .then(user => (user ? user.view(true) : null))
    .then(success(res))
    .catch(next)

export const verifyRegistration = (req, res, next) =>
  verify(req.body.token)
    .then(data => User.findById(data.id))
    .then(notFound(res))
    .then(user => (user ? user.set({ verified: true }).save() : null))
    .then(user => {
      return sendWelcomeMail(user)
    })
    .then(response => (response ? res.status(response.statusCode).end() : null))
    .catch(err => {
      /* istanbul ignore else */
      if (err.message === 'jwt expired') {
        res.status(410).json({
          valid: false,
          message: 'Lien de vérification expiré. Veuillez vous réinscrire.'
        })
      } else {
        next(err)
      }
    })

export const destroy = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then(user => (user ? user.remove() : null))
    .then(success(res, 204))
    .catch(next)

export const sendMessage = (req, res, next) =>
  sendPrivateMessage(req.body.message, req.body.fromUser, req.body.toUser)
    .then(success(res))
    .catch(next)
