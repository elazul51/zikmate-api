import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import {
  index,
  showMe,
  show,
  create,
  update,
  updatePassword,
  verifyRegistration,
  destroy,
  sendMessage
} from './controller'
import { schema } from './model'
export User, { schema } from './model'

const router = new Router()
const {
  _id,
  email,
  password,
  username,
  firstname,
  lastname,
  picture,
  newPicture,
  gender,
  birthdate,
  phone,
  address,
  instruments,
  presentation,
  role
} = schema.tree

/**
 * @api {get} /users Retrieve users
 * @apiName RetrieveUsers
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {ObjectId} _id User id.
 * @apiUse listParams
 * @apiSuccess {Object[]} users List of users.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', token({ required: true }), query({ _id }), index)

/**
 * @api {get} /users/me Retrieve current user
 * @apiName RetrieveCurrentUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess {Object} user User's data.
 */
router.get('/me', token({ required: true }), showMe)

/**
 * @api {get} /users/:id Retrieve user
 * @apiName RetrieveUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess {Object} user User's data.
 * @apiError 404 User not found.
 */
router.get('/:id', token({ required: true }), show)

/**
 * @api {post} /users Create user
 * @apiName CreateUser
 * @apiGroup User
 * @apiPermission master
 * @apiParam {String} access_token Master access_token.
 * @apiParam {String} email User's email.
 * @apiParam {String{8..}} password User's password.
 * @apiParam {String} [username] User's nickname.
 * @apiParam {String} [firstname] User's firstname.
 * @apiParam {String} [lastname] User's lastname.
 * @apiParam {String} [picture] User's picture.
 * @apiParam {String=user,admin} [role=user] User's role.
 * @apiSuccess (Sucess 201) {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Master access only.
 * @apiError 409 Email or username already registered.
 */
router.post(
  '/',
  master(),
  body({ email, password, username, firstname, lastname, picture, role }),
  create
)

/**
 * @api {put} /users/:id Update user
 * @apiName UpdateUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {String} [firstname] User's firstname.
 * @apiParam {String} [lastname] User's lastname.
 * @apiParam {String} [picture] User's picture.
 * @apiParam {String} [gender] User's gender.
 * @apiParam {Date} [birthdate] User's birthdate.
 * @apiParam {String} [address] User's address.
 * @apiParam {String} [phone] User's phone number.
 * @apiParam {Instrument[]} [instruments] User's instruments.
 * @apiParam {String} [presentation] User's presentation.
 * @apiSuccess {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Current user or admin access only.
 * @apiError 404 User not found.
 */
router.put(
  '/:id',
  token({ required: true }),
  body({
    firstname,
    lastname,
    newPicture,
    gender,
    birthdate,
    phone,
    address,
    instruments,
    presentation
  }),
  update
)

/**
 * @api {put} /users/:id/password Update password
 * @apiName UpdatePassword
 * @apiGroup User
 * @apiHeader {String} Authorization Basic authorization with email and password.
 * @apiParam {String{8..}} password User's new password.
 * @apiSuccess (Success 201) {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 Current user access only.
 * @apiError 404 User not found.
 */
router.put(
  '/:id/password',
  token({ required: true }),
  body({ password }),
  updatePassword
)

/**
 * @api {post} /users/verify Verify new user
 * @apiName VerifyUser
 * @apiGroup User
 * @apiParam {String} token User's verification token.
 * @apiSuccess (Success 201) {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User not found.
 */
router.post('/verify', master(), verifyRegistration)

/**
 * @api {delete} /users/:id Delete user
 * @apiName DeleteUser
 * @apiGroup User
 * @apiPermission admin
 * @apiParam {String} access_token User access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 401 Admin access only.
 * @apiError 404 User not found.
 */
router.delete('/:id', token({ required: true, roles: ['admin'] }), destroy)

/**
 * @api {post} /users/send-message Sends a private message to another user
 * @apiName SendMessageUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess (Success 201) {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 User not found.
 */
router.post('/send-message', token({ required: true }), sendMessage)

export default router
