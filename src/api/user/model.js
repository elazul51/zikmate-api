import crypto from 'crypto'
import bcrypt from 'bcrypt'
import randtoken from 'rand-token'
import mongoose, { Schema } from 'mongoose'
import mongooseKeywords from 'mongoose-keywords'
import uniqueValidator from 'mongoose-unique-validator'
import autopopulate from 'mongoose-autopopulate'
import { env } from '../../config'
import Friendship from '../friendship/model'

const roles = ['user', 'admin']
const genders = ['male', 'female']

const userSchema = new Schema(
  {
    email: {
      type: String,
      match: /^\S+@\S+\.\S+$/,
      index: true,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      maxlength: [
        255,
        "L'adresse mail doit contenir au maximum 255 caractères."
      ]
    },
    password: {
      type: String,
      required: true,
      minlength: [8, 'Le mot de passe doit contenir au minimum 8 caractères.'],
      maxlength: [
        255,
        'Le mot de passe doit contenir au maximum 255 caractères.'
      ]
    },
    username: {
      type: String,
      index: true,
      required: true,
      unique: true,
      trim: true,
      maxlength: [50, 'Le pseudo doit contenir au maximum 50 caractères.']
    },
    firstname: {
      type: String,
      required: true,
      trim: true,
      maxlength: [50, 'Le prénom doit contenir au maximum 50 caractères.']
    },
    lastname: {
      type: String,
      required: true,
      trim: true,
      maxlength: [50, 'Le nom doit contenir au maximum 50 caractères.']
    },
    gender: {
      type: String,
      enum: genders
    },
    birthdate: {
      type: Date
    },
    address: {
      type: Object
    },
    phone: {
      type: String,
      trim: true,
      maxlength: [
        30,
        'Le numéro de téléphone doit contenir au maximum 30 caractères.'
      ]
    },
    instruments: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Instrument'
      }
    ],
    presentation: {
      type: String,
      trim: true,
      maxlength: [
        5000,
        'La présentation doit contenir au maximum 5000 caractères.'
      ]
    },
    publications: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
      }
    ],
    friendships: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Friendship',
        autopopulate: {
          select: [
            '_id',
            'fromUser',
            'toUser',
            'accepted',
            'createdAt',
            'updatedAt'
          ],
          maxDepth: 2
        }
      }
    ],
    services: {
      facebook: String,
      github: String,
      google: String
    },
    role: {
      type: String,
      enum: roles,
      default: 'user'
    },
    picture: {
      type: String,
      trim: true
    },
    newPicture: {
      type: String
    },
    verified: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
)

userSchema.plugin(autopopulate)

userSchema.plugin(uniqueValidator, {
  message: '{VALUE} existe déjà'
})

userSchema.path('email').set(function (email) {
  if (!this.picture || this.picture.indexOf('https://gravatar.com') === 0) {
    const hash = crypto
      .createHash('md5')
      .update(email)
      .digest('hex')
    this.picture = `https://gravatar.com/avatar/${hash}?d=wavatar`
  }

  if (!this.username) {
    this.username = email.replace(/^(.+)@.+$/, '$1')
  }

  return email
})

userSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next()

  /* istanbul ignore next */
  const rounds = env === 'test' ? 1 : 9

  bcrypt
    .hash(this.password, rounds)
    .then(hash => {
      this.password = hash
      next()
    })
    .catch(next)
})

userSchema.pre('remove', function (next) {
  Friendship.remove({
    $or: [{ fromUser: this._id }, { toUser: this._id }]
  }).exec()
  next()
})

userSchema.methods = {
  view (full) {
    let view = {}
    let fields = [
      'id',
      'username',
      'firstname',
      'lastname',
      'email',
      'picture',
      'address'
    ]

    if (full) {
      fields = [
        ...fields,
        'gender',
        'phone',
        'birthdate',
        'instruments',
        'presentation',
        'createdAt',
        'role',
        'friendships'
      ]
    }

    fields.forEach(field => {
      view[field] = this[field]
    })

    return view
  },

  authenticate (password) {
    return bcrypt
      .compare(password, this.password)
      .then(valid => (valid ? this : false))
  }
}

userSchema.statics = {
  roles,

  createFromService ({
    service,
    id,
    email,
    username,
    firstname,
    lastname,
    picture,
    verified
  }) {
    return this.findOne({ email }).then(user => {
      if (user) {
        if (!user.services[service]) {
          user.services[service] = id
          user.verified = verified
          return user.save()
        }
        return user
      } else {
        const password = randtoken.generate(16)
        return this.create({
          services: { [service]: id },
          email,
          password,
          username,
          firstname,
          lastname,
          picture,
          verified
        })
      }
    })
  }
}

userSchema.plugin(mongooseKeywords, {
  paths: ['email', 'username', 'firstname', 'lastname']
})

const model = mongoose.model('User', userSchema)

export const schema = model.schema
export default model
