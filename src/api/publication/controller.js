import { success, notFound } from '../../services/response/'
import { Publication } from '.'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Publication.countDocuments({ author: query.author, origin: null })
    .then(totalCount => {
      return Promise.all([totalCount, Publication.find(query, select, cursor)])
    })
    .then(([totalCount, publications]) => {
      const result = {}
      result.data = publications.map(publication => publication.view())
      result.totalCount = totalCount
      return result
    })
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Publication.findById(params.id)
    .then(notFound(res))
    .then(publication => (publication ? publication.view() : null))
    .then(success(res))
    .catch(next)

export const create = ({ bodymen: { body } }, res, next) =>
  Publication.create(body)
    .then(publication => {
      let promises = [publication]
      if (body.origin) {
        promises.push(
          Publication.findOneAndUpdate(
            { _id: body.origin },
            {
              $push: { replies: publication.id }
            }
          )
        )
      }
      return Promise.all(promises)
    })
    .then(([publication]) => (publication ? publication.view() : null))
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params, publication }, res, next) =>
  Publication.findById(params.id)
    .then(notFound(res))
    .then(publication =>
      publication ? Object.assign(publication, body).save() : null
    )
    .then(publication => (publication ? publication.view() : null))
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Publication.findById(params.id)
    .then(notFound(res))
    .then(publication => (publication ? publication.remove() : null))
    .then(success(res, 204))
    .catch(next)
