import mongoose, { Schema } from 'mongoose'
import autopopulate from 'mongoose-autopopulate'

const publicationSchema = new Schema(
  {
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      index: true,
      required: true,
      autopopulate: { select: ['username', 'picture'] }
    },
    content: {
      type: String,
      required: true,
      trim: true,
      maxlength: [
        20000,
        'Le texte de la publication doit contenir au maximum 20000 caractères.'
      ]
    },
    origin: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Publication',
      operator: '$exists'
    },
    replies: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication',
        autopopulate: { select: ['author', 'content', 'createdAt'] }
      }
    ]
  },
  {
    timestamps: true
  }
)

publicationSchema.plugin(autopopulate)

publicationSchema.methods = {
  view () {
    let view = {}
    const fields = ['id', 'author', 'content', 'replies', 'createdAt']

    fields.forEach(field => {
      view[field] = this[field]
    })

    return view
  }
}

const model = mongoose.model('Publication', publicationSchema)

export const schema = model.schema
export default model
