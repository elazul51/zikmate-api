import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { index, show, create, update, destroy } from './controller'
import { schema } from './model'
export Publication, { schema } from './model'

const router = new Router()
const { author, content, origin } = schema.tree

/**
 * @api {get} /publications Retrieve publications
 * @apiName RetrievePublications
 * @apiGroup Publication
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {ObjectId} author Publication's author id.
 * @apiParam {ObjectId} origin Publication's origin id for a reply publication.
 * @apiUse listParams
 * @apiSuccess {Object[]} publications List of publications.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', token({ required: true }), query({ author, origin }), index)

/**
 * @api {get} /publications/:id Retrieve publication
 * @apiName RetrievePublication
 * @apiGroup Publication
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess {Object} publication Publication's data.
 * @apiError 404 Publication not found.
 */
router.get('/:id', token({ required: true }), show)

/**
 * @api {post} /publications Create publication
 * @apiName CreatePublication
 * @apiGroup Publication
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {ObjectId} author Publication's author id.
 * @apiParam {String} content Publication's content.
 * @apiParam {ObjectId} origin Publication's origin id for a reply publication.
 * @apiSuccess (Sucess 201) {Object} publication Publication's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.post(
  '/',
  token({ required: true }),
  body({ author, content, origin }),
  create
)

/**
 * @api {put} /publications/:id Update publication
 * @apiName UpdatePublication
 * @apiGroup Publication
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {String} [content] Publication's content.
 * @apiSuccess {Object} publication Publication's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Publication not found.
 */
router.put('/:id', token({ required: true }), body({ content }), update)

/**
 * @api {delete} /publications/:id Delete publication
 * @apiName DeletePublication
 * @apiGroup Publication
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Publication not found.
 */
router.delete('/:id', token({ required: true }), destroy)

export default router
