import { success } from '../../services/response/'
import { Chat } from '.'

export const index = (req, res, next) =>
  Chat.find()
    .then(success(res))
    .catch(next)
