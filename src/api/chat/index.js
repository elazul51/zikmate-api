import { Router } from 'express'
import { token } from '../../services/passport'
import { index } from './controller'
export Chat, { schema } from './model'

const router = new Router()

/**
 * @api {get} /chat Retrieve chat
 * @apiName RetrieveChat
 * @apiGroup Chat
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiUse listParams
 * @apiSuccess {Object[]} chat List of chat.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', token({ required: true }), index)

export default router
