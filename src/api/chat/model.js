import mongoose, { Schema } from 'mongoose'

const chatSchema = new Schema(
  {
    handle: {
      type: String,
      required: true,
      trim: true,
      maxlength: [50, 'Le pseudo doit contenir au maximum 50 caractères.']
    },
    message: {
      type: String,
      required: true,
      trim: true,
      maxlength: [1000, 'Le message doit contenir au maximum 1000 caractères.']
    }
  },
  {
    timestamps: true
  }
)

const model = mongoose.model('Chat', chatSchema)

export const schema = model.schema
export default model
