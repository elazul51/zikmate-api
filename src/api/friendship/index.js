import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { index, show, create, update, destroy } from './controller'
import { schema } from './model'
export Friendship, { schema } from './model'

const router = new Router()
const { fromUser, toUser, accepted } = schema.tree

/**
 * @api {get} /friendships Retrieve friendships
 * @apiName RetrieveFriendships
 * @apiGroup Friendship
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {ObjectId} fromUser Friendship's from user id.
 * @apiParam {Boolean} accepted Friendship's accepted status.
 * @apiUse listParams
 * @apiSuccess {Object[]} friendships List of friendships.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', token({ required: true }), query({ fromUser, toUser, accepted }), index)

/**
 * @api {get} /friendships/:id Retrieve friendship
 * @apiName RetrieveFriendship
 * @apiGroup Friendship
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess {Object} friendship Friendship's data.
 * @apiError 404 Friendship not found.
 */
router.get('/:id', token({ required: true }), show)

/**
 * @api {post} /friendships Create friendship
 * @apiName CreateFriendship
 * @apiGroup Friendship
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {ObjectId} fromUser Friendship's from user id.
 * @apiParam {ObjectId} toUser Friendship's to user id.
 * @apiSuccess (Sucess 201) {Object} friendship Friendship's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.post('/', token({ required: true }), body({ fromUser, toUser }), create)

/**
 * @api {put} /friendships/:id Update friendship
 * @apiName UpdateFriendship
 * @apiGroup Friendship
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiParam {Boolean} accepted Friendship's accepted status.
 * @apiSuccess {Object} friendship Friendship's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Friendship not found.
 */
router.put('/:id', token({ required: true }), body({ accepted }), update)

/**
 * @api {delete} /friendships/:id Delete friendship
 * @apiName DeleteFriendship
 * @apiGroup Friendship
 * @apiPermission user
 * @apiParam {String} access_token User access_token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Friendship not found.
 */
router.delete('/:id', token({ required: true }), destroy)

export default router
