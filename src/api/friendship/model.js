import mongoose, { Schema } from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import User from '../user/model'

const friendshipSchema = new Schema(
  {
    fromUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      index: true,
      required: true,
      autopopulate: {
        select: ['_id', 'username', 'firstname', 'lastname', 'email', 'picture']
      }
    },
    toUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      index: true,
      required: true,
      autopopulate: {
        select: ['_id', 'username', 'firstname', 'lastname', 'email', 'picture']
      }
    },
    accepted: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
)

friendshipSchema.index(
  { fromUser: 1, toUser: 1 },
  { unique: true, dropDups: true }
)

friendshipSchema.plugin(autopopulate)

friendshipSchema.pre('remove', function (next) {
  User.update(
    { friendships: this._id },
    { $pull: { friendships: this._id } },
    { multi: true }
  ).exec()
  next()
})

friendshipSchema.methods = {
  view () {
    let view = {}
    const fields = [
      '_id',
      'fromUser',
      'toUser',
      'accepted',
      'createdAt',
      'updatedAt'
    ]

    fields.forEach(field => {
      view[field] = this[field]
    })

    return view
  }
}

const model = mongoose.model('Friendship', friendshipSchema)

export const schema = model.schema
export default model
