import { success, notFound } from '../../services/response/'
import { sendFriendRequest } from '../../services/sendgrid'
import { Friendship } from '.'
import { User } from '../user'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Friendship.countDocuments({
    $or: [{ fromUser: query.fromUser }, { toUser: query.toUser }],
    accepted: query.accepted
  })
    .then(totalCount => {
      return Promise.all([
        totalCount,
        Friendship.find(
          {
            $or: [{ fromUser: query.fromUser }, { toUser: query.toUser }],
            accepted: query.accepted
          },
          select,
          cursor
        )
      ])
    })
    .then(([totalCount, friendships]) => {
      const result = {}
      result.data = friendships.map(friendship => friendship.view())
      result.totalCount = totalCount
      return result
    })
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Friendship.findById(params.id)
    .then(notFound(res))
    .then(friendship => (friendship ? friendship.view() : null))
    .then(success(res))
    .catch(next)

export const create = ({ bodymen: { body } }, res, next) =>
  Friendship.create(body)
    .then(friendship => {
      return Promise.all([
        friendship,
        User.findOneAndUpdate(
          { _id: friendship.fromUser },
          {
            $push: { friendships: friendship.id }
          }
        ),
        User.findOneAndUpdate(
          { _id: friendship.toUser },
          {
            $push: { friendships: friendship.id }
          }
        )
      ])
    })
    .then(([friendship, fromUser, toUser]) =>
      Promise.all([friendship, sendFriendRequest(fromUser, toUser)])
    )
    .then(([friendship]) => (friendship ? friendship.view() : null))
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params, friendship }, res, next) =>
  Friendship.findById(params.id)
    .then(notFound(res))
    .then(friendship =>
      friendship ? Object.assign(friendship, body).save() : null
    )
    .then(friendship => (friendship ? friendship.view() : null))
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Friendship.findById(params.id)
    .then(notFound(res))
    .then(friendship => (friendship ? friendship.remove() : null))
    .then(success(res, 204))
    .catch(next)
